/* eslint-disable */
import Vue from "vue";
import App from "./App";
import router from "./router";
Vue.prototype.$eventHub = new Vue();
import VueCookies from "vue-cookies";
Vue.use(VueCookies);

import axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, axios);

import Datetime from "vue-datetime";
import "vue-datetime/dist/vue-datetime.css";

Vue.use(Datetime);
new Vue({
  el: "#app",
  router,
  components: { App },
  template: "<App/>"
});
