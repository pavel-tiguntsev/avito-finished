import Vue from "vue";
import Router from "vue-router";
import FormToAdd from "@/components/FormToAdd";
import Table from "@/components/Table";
import login from "@/components/login";
import editad from "@/components/editad";
import prices from "@/components/prices";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Table",
      component: Table
    },
    {
      path: "/add",
      name: "FormToAdd",
      component: FormToAdd
    },
    {
      path: "/login",
      name: "login",
      component: login
    },
    {
      path: "/prices",
      name: "prices",
      component: prices
    },
    {
      path: "/editad",
      name: "editad",
      component: editad
    }
  ]
});
