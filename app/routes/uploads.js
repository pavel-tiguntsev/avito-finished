
function randomString(len, charSet) {
  charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var randomString = '';
  for (var i = 0; i < len; i++) {
      var randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz,randomPoz+1);
  }
  return randomString;
}

module.exports = function(app) {

  app.post('/upload', function(req, res) {
    if (!req.files)
      return res.status(400).send('No files were uploaded.');
    console.log(req.body)
    var c = 0;
    var imgs = [];
    for(var key in req.files) {
      c += 1;
      var file = req.files[key];
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      var fullFileName = Date.now() + randomString(2) + '.' + ext;
      imgs.push(fullFileName)
      file.mv('app/images/' +  fullFileName, function(err) {
        
        if (err)
          return res.status(500).send(err);
        // return
     })
    }
      console.log(imgs);
      
     res.json({imgs});
      
    });
  }