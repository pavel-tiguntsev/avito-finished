const Ad = require("../models/Ad");
const Price = require("../models/Price");
var xml = require("xml");

function renameKeys(obj, newKeys) {
  const keyValues = Object.keys(obj).map(key => {
    const newKey = newKeys[key] || key;
    return { [newKey]: obj[key] };
  });
  return Object.assign({}, ...keyValues);
}

module.exports = function(app) {
  app.post("/ad", (req, res) => {
    var newAd = new Ad.m(req.body);

    newAd.save((err, s) => {
      console.log(req.body);
      res.send(req.body);
    });
  });

  app.get("/ad/:id", (req, res) => {
    var id = req.params.id;
    console.log(1, id);
    Ad.m.findOne({ _id: id }, (err, docs) => {
      console.log(2, docs);
      res.json(docs);
    });
  });

  app.put("/ad/:id", (req, res) => {
    var id = req.params.id;
    Ad.m.update({ _id: id }, { $set: req.body }, () => {
      res.send("updated");
    });
  });

  app.put("/ad", (req, res) => {
    var newAd = new Ad.m(req.body);
    newAd.save((err, s) => {
      console.log(req.body);
      res.send(req.body);
    });
  });

  app.delete("/deleteAd", (req, res) => {
    var id = req.query.id;
    console.log(id);
    Ad.m.remove({ _id: id }, err => {
      if (err) throw err;
      res.send("deleted");
    });
  });

  app.get("/ads", (req, res) => {
    var userId = req.user._id;
    Ad.m.find({ userId: req.user._id }, (err, docs) => {
      res.json(docs);
    });
  });

  app.get("/prices", (req, res) => {
    Price.find({}, (err, docs) => {
      res.json(docs);
    });
  });

  app.delete("/deletePrice", (req, res) => {
    var id = req.query.id;
    console.log(id);
    Price.remove({ _id: id }, err => {
      if (err) throw err;
      res.send("deleted");
    });
  });

  app.put("/price", (req, res) => {
    var id = req.body.id;
    var newPrice = req.body.price
    Price.update({ _id: id }, {$set: {value: newPrice}}, () => {
      res.send("updates");
    });
  });

  app.post("/prices", (req, res) => {
    var name = req.body.name
    Price.findOne({"name": name}, (err, docs) => {
      if (docs == null) {
        var price = new Price();
        price.name = name;
        price.value = req.body.value;
        price.save(err => {
          if (err) console.log(err);
          res.send('created');
        })
      }
    });

  });

  app.get("/ads-xlm/:id", (req, res) => {
    Price.find({}, (err, prices) => {
      var id = req.params.id;
      var xmlDump = '<Ads formatVersion="3" target="Avito.ru">';
      prices_dict = {}
      prices.forEach(function(item) {
        prices_dict[item.name] = item.value;
      });
      

      Ad.m.find({ userId: id }, (err, docs) => {
        for (var i = 0; i < docs.length; i++) {
          var infoArray = [];
          infoArray.push({ Id: docs[i]._id.toString() });
  
          if (docs[i].Title != undefined) {
            infoArray.push({ Title: docs[i].Title.toString() });
          }
          if (docs[i].Category != undefined) {
            infoArray.push({ Category: docs[i].Category.toString() });
          }
          if (docs[i].AllowEmail != undefined) {
            infoArray.push({ AllowEmail: docs[i].AllowEmail.toString() });
          }
          if (docs[i].Subway != undefined) {
          }
          if (docs[i].Region != undefined) {
            infoArray.push({ Region: docs[i].Region.toString() });
          }
          if (docs[i].Description != undefined) {
            infoArray.push({ Description: { _cdata: docs[i].Description } });
          }
  
          if (docs[i].DateBegin != undefined) {
            infoArray.push({ DateBegin: docs[i].DateBegin.toString() });
          }
          if (docs[i].DateEnd != undefined) {
            infoArray.push({ DateEnd: docs[i].DateEnd.toString() });
          }
          if (docs[i].Price != undefined) {
                infoArray.push({ Price: prices_dict[docs[i].Price] });
          }
          var images = [];
          for (var j = 0; j < docs[i].Images.length; j++) {
            images.push({
              Image: {
                _attr: {
                  url: "http://avito.cu.market/images/" + docs[i].Images[j]
                }
              }
            });
          }
  
          infoArray.push({ Images: images });
          xmlDump += xml({ Ad: infoArray }).toString();
        }
  
        xmlDump += "</Ads>";
        // xmlDump += xmlDump + "</Ads>";
        res.set("Content-Type", "text/xml");
        res.send(xmlDump);
      });
    })


  });

  app.get("/userFeed/:id", (req, res) => {
    Ad.m.find({ userId: req.params.id }, (err, docs) => {
      if (err) throw err;
      res.json(docs);
    });
  });

  app.get("/json-ads", (req, res) => {
    Ad.m.find({}, (err, docs) => {
      if (err) throw err;
      res.json(docs);
    });
  });

  app.get("/getSctruct", (req, res) => {
    SctructJSON = Ad.structJSON;

    mapInputTypes = {
      String: "text",
      Number: "Number",
      "[String]": "[String]"
    };

    res.json({
      struct: SctructJSON,
      map: mapInputTypes
    });
  });
};
