const User = require("../models/User");

module.exports = function(app) {
  app.post("/login", (req, res) => {
    console.log(req.body);
    var password = req.body.password;
    if (!password || !req.body.email) {
      res.sendStatus(403);
      return;
    }
    User.findOne({ email: req.body.email }, (err, doc) => {
      if (err) throw err;
      if (doc != null) {
        if (doc.validPassword(password)) {
          console.log(doc);
          res.send({
            user: doc,
            response: "Успешно зашли!"
          });
        } else {
          res.send({ response: "Неверный пароль!" });
        }
      } else {
        res.send({ response: "Пользователь не найден!" });
      }
    });
  });
};
