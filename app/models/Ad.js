var mongoose = require("mongoose");

var adScruct = {
  Category: String,
  Title: String,
  Price: String,
  Description: String,
  DateBegin: String,
  DateEnd: String,
  Images: [String],
  Region: String,
  Subway: String,
  AdStatus: String,
  AllowEmail: String,
  userId: String
};

function structJSON(struct) {
  var json = {};
  for (name in struct) {
    var type = struct[name].name;
    if (type == undefined) {
      json[name] = "[" + struct[name][0].name + "]";
    } else {
      json[name] = struct[name].name;
    }
  }
  return json;
}

var adSchema = mongoose.Schema(adScruct);

module.exports.m = mongoose.model("Ad", adSchema);
module.exports.structJSON = structJSON(adScruct);
