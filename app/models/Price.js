var mongoose = require("mongoose");
var bcrypt = require("bcrypt-nodejs");

var priceSchema = mongoose.Schema({
  name: String,
  value: Number
});


module.exports = mongoose.model("Price", priceSchema);
