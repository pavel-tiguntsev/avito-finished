var mongoose = require("mongoose");

var adScruct = {
  Title: String,
  Images: [String],
  Category: String,
  Price: Number,
  Description: String,
  DateBegin: String,
  DateEnd: String,
  Region: String,
  Subway: String,
  // AdStatus: String,
  AllowEmail: {type: String, default: "Да"},
  userId: String
};

function structJSON(struct) {
  var json = {};
  for (name in struct) {
    var type = struct[name].name;
    if (type == undefined) {
      json[name] = "[" + struct[name][0].name + "]";
    } else {
      json[name] = struct[name].name;
    }
  }
  return json;
}

var adSchema = mongoose.Schema(adScruct);

module.exports.m = mongoose.model("Ad", adSchema);
module.exports.structJSON = structJSON(adScruct);
