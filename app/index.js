//Load config
const config = require("./config.json");

//Server
const fileUpload = require('express-fileupload');
const express = require("express");
const app = express();
const port = config.appPort;
const bodyParser = require("body-parser");

//Database init
const murl = config.murl;
const dbName = config.db;
const mongoose = require("mongoose");
mongoose.connect(murl + "/" + dbName);

app.use(fileUpload());
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use('/images', express.static(__dirname + '/images'))
app.use(express.static(__dirname + '/../table-front/dist'))


var login = require("./routes/login.js");
var ads = require("./routes/ads.js");
var uploads = require("./routes/uploads.js");
ads(app);
login(app);
uploads(app);

app.listen(port, () => {
  console.log(__dirname)
  console.log("Server running on ", port);
});
