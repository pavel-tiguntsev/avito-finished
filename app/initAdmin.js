const config = require("./config.json");

const murl = config.murl;
const dbName = config.db;
const mongoose = require("mongoose");
mongoose.connect(murl + "/" + dbName);

const User = require("./models/User");
console.log("running with argv: ", process.argv);

var admin = new User();

admin.email = process.argv[2] || "dev0@cryptouniverse.io";
admin.password =
  admin.generateHash(process.argv[3]) || admin.generateHash("qsawqs88");
admin.role = "admin";

admin.save((err, doc) => {
  if (err) throw err;
  console.log(doc);
  process.exit();
});
